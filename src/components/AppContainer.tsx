import React from 'react';
import MainContainer from './MainContainer';
import Header from './Header';
import Footer from './Footer';

class AppContainer extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <MainContainer/>
                <Footer/>
            </div>
        );
    }
}

export default AppContainer;
