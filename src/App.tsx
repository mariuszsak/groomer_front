import React from 'react';
import './App.css';
import AppContainer from './components/AppContainer';

const App = () => {
  return (
    <div className="main">
      <AppContainer />
    </div>
  );
};

export default App;
